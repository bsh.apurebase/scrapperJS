import puppeteer from "puppeteer";

let mainData = '';
let mainDataArray = [];

const getMedicalData = async () => {
    const browser = await puppeteer.launch({
        headless: false,
        defaultViewport: null,
    });

    const page = await browser.newPage();

    let pageNumber = 1;
    const toScrapUrl = scrappingWebUrl(pageNumber);
    await page.goto(toScrapUrl, {
        waitUntil: "domcontentloaded",
    });

    const numberOfPages = await page.evaluate(() => {
        const navigationList = document.querySelector(".pagination");
        if (navigationList) {
            const lastPageUrl = navigationList.lastElementChild.getElementsByTagName('a')[0].getAttribute('hx-get');
            const match = lastPageUrl.match(/(?:\/?PageNumber=)([1-9]\d*)/);
            let number = 0;
            if ( match ) {
                number = Number(match[1]);
            }
            return number;
        } else {
            return 1;
        }

    });

    for (let i= 1; i <= numberOfPages; i++) {
        const toScrapUrl = scrappingWebUrl(i);
        await page.goto(toScrapUrl, {
            waitUntil: "domcontentloaded"
        });

        const dataCard = await page.evaluate(() => {
            const mapFieldNames = {
                'Naam': 'Name',
                'RIZIV-nr': 'RIZIV-nr',
                'Beroep': 'Profession',
                'Conv': 'Conv',
                'Kwalificatie': 'Qualification',
                'Kwal. datum': 'Exam Date',
                'Werkadres': 'Address'
            };
           const dataList = document.querySelectorAll('.card');
           return Array.from(dataList).map((card) => {

               const fieldListSelector = card.querySelectorAll('.row');
               let address = 1;
               return Array.from(fieldListSelector).reduce((acc, row) => {
                  const fieldRow = row.getElementsByTagName('small');
                  let fieldName = mapFieldNames.hasOwnProperty(fieldRow[0].innerText) ? mapFieldNames[fieldRow[0].innerText]: fieldRow[0].innerText;
                  let fieldValue = undefined;
                  if (fieldName === 'Address') {
                      const fullAddress = (fieldRow[1]?.innerText)  ?
                          fieldRow[1]?.innerText + (fieldRow[2]?.innerText ?
                              '//' + fieldRow[2]?.innerText : '')
                          :fieldRow[2]?.innerText;

                      fieldValue = address === 1 ? [fullAddress] : [...acc[fieldName], fullAddress];
                      address++;
                  } else {
                      fieldValue = fieldRow[1]?.innerText;
                  }

                  return {
                      ...acc,
                      [fieldName]: fieldValue
                  }
               }, {});
           })
        });
      const formattedDataCard = dataCard.map(data => {
          let OfficialId;
          const [LastName, FirstName] = data.Name.split(',');
          const qualificationCodeMatch = data['Qualification'].match(/[0-9]+/);
          let qualifcationCode;
          if (qualificationCodeMatch) {
              qualifcationCode = qualificationCodeMatch[0];
          }
          if (qualifcationCode) {
              OfficialId = data['RIZIV-nr'] + qualifcationCode;
          }
          const dataObj = {
              ...data,
              FirstName,
              LastName,
              OfficialId,
          };
          delete dataObj['Address'];
          const addressList = data['Address'];
          let toReturnDataObj = [];
          if (addressList) {
              for (const address of addressList) {
                  let WorkPlaceName, WorkPlaceAddress , PostalCode , Region;
                  let remainingAddress;
                  if (address !== 'Geen hoofdwerkadres gekend') {
                      if (address.includes('//')) {
                          let [workPlaceName, remAddress] = address.split('//');
                          WorkPlaceName = workPlaceName;
                          remainingAddress = remAddress;
                      } else {
                          remainingAddress = address;
                      }
                      if (remainingAddress) {
                          const [workPlaceAddress, postRegion] = remainingAddress.split("\n");
                          WorkPlaceAddress = workPlaceAddress;
                          if (postRegion) {
                              PostalCode = postRegion.match(/[0-9]+/)[0];
                              Region = postRegion.split(/[0-9]+/)[1];
                          }
                      }
                  }
                  toReturnDataObj = [...toReturnDataObj, {...dataObj, WorkPlaceName, WorkPlaceAddress,PostalCode,Region} ]
              }
          }
          return toReturnDataObj.length > 0 ? toReturnDataObj : dataObj;
      });
      const flattenedFormattedDataSet = formattedDataCard.flat(2);
      mainDataArray = [...mainDataArray, ...flattenedFormattedDataSet];
    }
    mainData = arrayOfObjectToPsv(mainDataArray);
    await page.evaluate((evalVar) => {
        let hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(evalVar);
        hiddenElement.target = '_blank';

        //provide the name for the CSV file to be downloaded
        hiddenElement.download = `purebaseData${new Date().getTime() * Math.random()}.csv`;
        hiddenElement.click();
    }, mainData);

    await browser.close();
}

function arrayOfObjectToPsv(data){
    const psvRows = [];
    const headers = Object.keys(data[0]);
    psvRows.push(headers.join('|'));
    for (const row of data) {
        const values = headers.map(header => {
            const val = row[header]
            return `"${val}"`;
        });
        psvRows.push(values.join('|'));
    }
    return psvRows.join('\n');
}

function scrappingWebUrl(pageNumber) {
    const professionCode = 10;
    const qualificationCode = '096';
    const dynamicFilter = `&Form.Name=&Form.FirstName=&Form.Profession=${professionCode}&Form.Specialisation=&Form.ConventionState=&Form.Location=&Form.NihdiNumber=&Form.Qualification=${qualificationCode}&Form.NorthEastLat=&Form.NorthEastLng=&Form.SouthWestLat=&Form.SouthWestLng=&Form.LocationLng=&Form.LocationLat=`;
    return `https://webappsa.riziv-inami.fgov.be/silverpages/Home/SearchHcw/?PageNumber=${pageNumber}${dynamicFilter}`
}

await getMedicalData();
